#!/usr/bin/env bash

set -euET -o pipefail

# Create git bundle for easy export from GitPod
git bundle create gitpod.bundle --all
base64 gitpod.bundle > gitpod.bundle.b64

if ! which dot >/dev/null 2>&1; then
  sudo apt -y install graphviz
fi

dot -Tpng protocol.dot > protocol.png

/ide/ligo run test tests/subneg.test.mligo

printf \\033'[0;32m%s'\\033'[m'\\n "test.sh exited successfully."
