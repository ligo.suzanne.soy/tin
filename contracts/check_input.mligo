#import "todo.mligo" "Todo"
#import "settings.mligo" "Settings"

let checkHash (xash : bytes) = 
  if Bytes.length xash > Settings.longest_hash then
    (* check that the bytes are not too long for a hash
       (to avoid denial of service attacks which force
       excessive gas consumption by posting a huge bytestring
       instead of a hash). *)
    (failwith "BadHash" : unit)
  else 
    ()

let checkArgument (argument : bytes) =
  if Bytes.length argument > Settings.longest_argument then
    (* check that the bytes are not too long for a hash
       (to avoid denial of service attacks which force
       excessive gas consumption by posting a huge bytestring
       instead of a hash). *)
    (failwith "BadArg" : unit)
  else
    ()

let checkLog2MemorySize (log2MemorySize : nat) =
  if log2MemorySize < 10n || log2MemorySize > 64n then
    (* VM memory size is too small or too big. *)
    failwith "BadMemSize"
  else
    ()


let checkVm ({ rootMerkleHash ; log2MemorySize } : Todo.vm) =
  let () = checkHash rootMerkleHash in
  let () = checkLog2MemorySize log2MemorySize in
  ()

let operationRequest ({amount=amnt;recipient;argument;inputHash;outputHash;vmId;newVmRootHashA} : Todo.operationRequest) =
  let _ = amnt in
  let _ = recipient in
  let () = checkArgument argument in
  let () = checkHash inputHash in
  let () = checkHash outputHash in
  let _ = vmId in
  let () = checkHash newVmRootHashA in
  ()
