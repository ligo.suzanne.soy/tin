#import "merkle.mligo" "Merkle"
#import "todo.mligo" "Todo"

(* TODO: sanitize user inputs, check that the VM's memory size is not too small
   (>0 and enough space for I/O) *)

type step = {
    vmId : Todo.vmId ;
    litigationId : Todo.litigationId ;
    (* Instruction pointer (should noramlly be at index zero and its value should be the index of oldA) *)
    oldInstructionPointer : Merkle.leaf_ ;
    (* Subneg is an OISC architecture (One-Instruction Set Computer), there is no opcode since
       all instructions are the same. However there are three operands: a, b and c. *)
    oldA : Merkle.leaf_ ;
    oldB : Merkle.leaf_ ;
    oldC : Merkle.leaf_ ;
    (* The first two operands are interpreted as pointers to other memory locations,
       the pointed-to locations follow. *)
    oldMemA : Merkle.leaf_ ; (* mem[a] *)
    oldMemB : Merkle.leaf_ ; (* mem[b] *)
}

(* Check that this value is > 0 and < memorySize *)
let assertPointer (vm : Todo.vm) (ptr : Merkle.leaf) =
  match Michelson.is_nat ptr.value with
  | None -> failwith "BadPtr"
  | Some v ->
    if v >= Todo.memorySize vm then
      failwith "BadPtr"
    else
      v

let assertValidValue (vm : Todo.vm) (val : int) =
  ()

let step (vm : Todo.vm) ({ vmId=_; litigationId=_; oldInstructionPointer; oldA; oldB; oldC; oldMemA; oldMemB } : step) : Todo.hash =
  let merkle : Merkle.sparseVector = Merkle.emptySparseVector in
  
  (* The IP is at 0 *)
  let oldInstructionPointer = Merkle.withIndex oldInstructionPointer 0n in
  
  (* Check that the IP is a valid pointer (positive integer < memory size) *)
  let oldInstructionPointerValue = assertPointer vm oldInstructionPointer in
  
  (* Use the IP's value as the address of a, b and c *)
  let oldA = Merkle.withIndex oldA (oldInstructionPointerValue + 0n) in
  let oldB = Merkle.withIndex oldB (oldInstructionPointerValue + 1n) in
  let oldC = Merkle.withIndex oldC (oldInstructionPointerValue + 2n) in
  
  (* oldA and oldB should be pointers (positive integers < memory size) *)
  let oldAValue = assertPointer vm oldA in
  let oldBValue = assertPointer vm oldB in
  (* Use the value of pointers a and b as the address of mem[a] and mem[b] *)
  let oldMemA = Merkle.withIndex oldMemA oldAValue in
  let oldMemB = Merkle.withIndex oldMemB oldBValue in

  (* Load the merkle proofs into a sparse representation of a merkle tree.
     That way, we can operate on the tree as if we knew all the nodes and leaves
     (as long as we don't actually access a node or leaf that we don't know). *)
  (* These also check that the merkle hash is correct according to the VM's rootMerkleHash. *)
  (* These also check that the indexes are within bounds. *)
  let merkle = Merkle.checkAndLoadPath vm merkle oldInstructionPointer in
  let merkle = Merkle.checkAndLoadPath vm merkle oldA in
  let merkle = Merkle.checkAndLoadPath vm merkle oldB in
  let merkle = Merkle.checkAndLoadPath vm merkle oldC in
  let merkle = Merkle.checkAndLoadPath vm merkle oldMemA in
  let merkle = Merkle.checkAndLoadPath vm merkle oldMemB in

  (* Check that the operands are valid values (integers within the range minValue..maxValue). *)
  let () = assertValidValue vm oldMemA.value in
  let () = assertValidValue vm oldMemB.value in
  let () = assertValidValue vm oldMemB.value in

  (* mem[b] := mem[b] - mem[a] *)
  let merkle, newMemB = Merkle.setPath vm merkle oldMemB.index (oldMemB.value - oldMemA.value) in

  (* ip := (if mem[b] < 0 then c else ip + 1) *)
  let newInstructionPointerValue =
    if (newMemB.value <= 0) then
      (* In this implementation of subneg, if the subtraction modifies the value of the c operand
         (by writing directly at its memory location), the jump destination is unaffected. To do an
         indirect jump, simply have two instructions: the first one modifies the value of the c operand
         of the second one, and the second one is just an unconditional jump. This avoids having to
         "read" from the post-subtraction memory in this contract. *)
      oldC.value
    else
      (* TODO: wrap around behaviour? *)
      let memorySize = Bitwise.shift_left 1n vm.log2MemorySize in
      let newInstructionPointer = oldInstructionPointerValue + 3n in
      (* wrap around *)
      int (newInstructionPointer mod (memorySize - 2n))
  in
  (* The instruction pointer register is stored in a specific memory location. *)
  let merkle, _newInstructionPointer =
    Merkle.setPath vm merkle oldInstructionPointer.index newInstructionPointerValue in
  (* return the root hash of the Merkle tree *)
  match Map.find_opt 1n merkle with
  (* Can't find the root hash of the Merkle tree *)
  | None -> failwith "NoRoot" (* internal error, can't happen *)
  | Some rootHash -> rootHash
