#import "merkle.mligo" "Merkle"
#import "todo.mligo" "Todo"
#import "settings.mligo" "Settings"
#import "01_start_transfer.mligo" "StartTransfer"
#import "02_start_litigation.mligo" "StartLitigation"
#import "03_trace_dichotomy.mligo" "TraceDichotomy"
#import "04_step.mligo" "Step"
#import "08_settle.mligo" "Settle"
#import "09_finish_transfer.mligo" "FinishTransfer"

let emptyStorage = {
    nextId            = 0n ;
    pendingOperations = (Big_map.empty : (nat, Todo.pendingOperation) big_map) ;
    litigations       = (Big_map.empty : (nat, Todo.litigation) big_map) ;
    vms               = (Big_map.empty : (nat, Todo.vm) big_map) ;
 }

type parameter =
  | StartTransfer     of StartTransfer.param
  | StartLitigation   of StartLitigation.param
  
  (* state of the litigation:
     traceRangeStart = initially 0
     traceRangeEnd   = initially max(length of A trace, length of B trace) *)

  | TraceDichotomy     of TraceDichotomy.param

  (* Possible cases:
     - the first observed divergence in the trace is at the step 0 (disagreement
       on the initial state of the memory)
     - the first observed divergence in the trace is at another step (disagreement
       on the execution of a single step)
     - there is no observed divergence in the trace (the initiator of the litigation lied about
       having a disagreement, maybe this can be checked before in the StartLitigation step instead). *)

  | Step              of Step.param

  | Settle            of Settle.param
  | FinishTransfer    of FinishTransfer.param


(* TODO: have a proper error mode, for:
   out-of-bounds pointer (TODO: check those)
   timeout (TODO: have a customizable "max program duration" which triggers a timeout, to avoid infinite loops)
   ...? *)

let main (action, store : parameter * Todo.storage) : Todo.return =
  match action with
    StartTransfer   (p) -> StartTransfer.entryPoint   store p
  | StartLitigation (p) -> StartLitigation.entryPoint store p
  | TraceDichotomy  (p) -> TraceDichotomy.entryPoint  store p
  | Step            (p) -> Step.entryPoint            store p
  | Settle          (p) -> Settle.entryPoint          store p
  | FinishTransfer  (p) -> FinishTransfer.entryPoint  store p