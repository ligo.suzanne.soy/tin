let reverse (type a) (lst : a list) =
  List.fold_left (fun (acc,x : a list * a) -> x :: acc) ([] : a list) lst
