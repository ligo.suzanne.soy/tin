#import "todo.mligo" "Todo"
#import "big_map_utils.mligo" "BigMapUtils"

type param = nat

let entryPoint (store : Todo.storage) (id : param) : Todo.return =
  let transfer_to_do, pendingOperations = BigMapUtils.pop id store.pendingOperations in
  let store = { store with pendingOperations = pendingOperations } in
  match transfer_to_do with
  | None -> (failwith "Bad ID" : Todo.return)
  (* TODO: bug in LIGO? amount seems to be a global keyword but can still be a var name *)
  | Some { operationRequest = { amount=amnt; recipient; argument; inputHash=_; outputHash=_; vmId=_; newVmRootHashA=_ };
           timelock } ->
    if timelock > Tezos.now then
      (failwith "Wait" : Todo.return)
    else
      let contract : bytes contract = match (Tezos.get_contract_opt recipient : bytes contract option) with Some c -> c | None -> failwith "BadAddr" in
      let op = Tezos.transaction argument amnt contract in
      let ops = [op] in
      (ops, store)
