#import "todo.mligo" "Todo"
#import "step.mligo" "Step"

type param = Step.step

let entryPoint (store : Todo.storage) (step : param) : Todo.return =
  (* TODO *)
  match Big_map.find_opt step.vmId store.vms, Big_map.find_opt step.litigationId store.litigations with
    | Some vm, Some (AwaitingStep dichotomy) ->
      let new_hash = Step.step vm step in
      if new_hash = dichotomy.traceEndHashA then
        (* A was correct, the litigation was invalid *)
        (* TODO: give B a fine, discard litigation. *)
        let ops = ([] : operation list) in
        (ops, store)
      else
        (*  *)
        let ops = ([] : operation list) in
        (ops, store)
    | Some _, _ ->
      (* This entry point needs the VM/litigation to be in the AwaitingStep state. *)
      failwith "BadStep"
    | None, _ ->
      (* Could not find the VM associated with this index. *)
      failwith "BadVMIdx"
