type litigation =
  | Step1 of unit
  | Step2 of unit
  | Step3 of unit

type storage = {
    nextId            : nat ;
    litigations       : (nat, litigation) big_map ;
}

let main (_, _ : unit * unit) : (operation list * unit) =
  ([] : operation list), ()