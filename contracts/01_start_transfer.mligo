#import "todo.mligo" "Todo"
#import "check_input.mligo" "CheckInput"
#import "utils.mligo" "Utils"
#import "settings.mligo" "Settings"

type param = Todo.operationRequest

let entryPoint (store : Todo.storage) (operationRequest : param) : Todo.return =
  let () = CheckInput.operationRequest operationRequest in
  let store, id = Utils.fresh_id store in
  let timelock = Tezos.now + int Settings.transfer_delay in
  let pendingOperation = {
    operationRequest = operationRequest ;
    timelock         = timelock  ;
  } in
  let store = {
    store with
    pendingOperations = Big_map.update id (Some pendingOperation) store.pendingOperations ;
  } in
  let ops = ([] : operation list) in
  (ops, store)
