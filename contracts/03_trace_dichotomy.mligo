#import "todo.mligo" "Todo"
#import "merkle.mligo" "Merkle"

(* Do a dichotomy between traceRangeStart and traceRangeEnd, to find a
   first point in the execution at which the traces differ (an agreed-upon
   state followed by a state on which the two parties disagree).
   
   Once both parties have supplied their midpoint hash:
   - if the two midpoint hashes are different then the first divergence is
     in the first half of the traceRangeStart..traceRangeEnd interval,
   - otherwise it is in the second half.
   
   When the range collapses to adjacent positions in the trace, we move on to the next step.
   
   Invariant:
   - traceRangeStart indicates a point in the trace on which both parties agree 
   - traceRangeEnd indicates a point in the trace on which the parties disagree *)

(* Examples, x indicates an incorrect state in the trace (in these examples, A is always right but B may cheat):
   A: 1 2 3 4 5
   B: 1 2 3 4 5
   No dichotomy (traces both end with the same result "5")
   A: 1 2 3 4 5
   B: 1 x 3 x 5
   No dichotomy (B cheated, but both traces end with "5" so the cheating doesn't actually affect the end result)
   A: 1 2 3 4 5
   B: 1 x 3 x x
   dichotomy: in intervals [1..5 vs. 1..x] look at 3 vs. 3: there must be a [agree,disagree] pair of consecutive steps in the second half [3..5 vs. 3..x]
              in intervals [3..5 vs. 3..x] look at 4 vs. x: there must be a [agree,disagree] pair of consecutive steps in the first half [3..4 vs. 3..x]
              Found a [good,bad] pair of consecutive steps: [3,4] vs. [3,x].
              We can now make one evaluation step starting from 3 and see if the result should be 4 or x.
   A: 1 2 3 4 5
   B: 1 2 x x x
   dichotomy: in intervals [1..5 vs. 1..x] look at 3 vs. x: there must be a [agree,disagree] pair of consecutive steps in the first half [1..3 vs. 1..x]
              in intervals [1..3 vs. 1..x] look at 2 vs. 2: there must be a [agree,disagree] pair of consecutive steps in the second half [2..3 vs. 2..x]
              Found a [good,bad] pair of consecutive steps: [2,3] vs. [2,x].
              We can now make one evaluation step starting from 2 and see if the result should be 3 or x. *)
(* Formal proof sketch:
   We are looking for any pair of consecutive states for which we have [agree, disagree] in the execution traces.
   
   So let's say A thinks the execution trace is [1 2 3 4 5] and B thinks the trace is [1 x 3 y z]. Letters indicate
   incorrect states (when B cheats and tweaks the memory to alter the end result), numbers indicate correct states
   (in reality these would be Merkle root hashes of the entire memory of the VM, and these hashes would be computed
   on-demand instead of hashing the entire memory at every step / instead of updating the Merkle tree for the changes
   made to the memory at every step).
   We start with the [agree … unknown … disagree] interval [1..5 vs. 1..z]
   We do a dichotomy by looking in the middle, 3 vs. 3
   It's an agree state. So we now have two intervals: [1..3 vs. 1..3] which is [agree … unknown … agree],
   and [3..5 vs. 3..z] which is [agree … unknown … disagree].
   We don't care about the unknown parts (sure, the first unknown in [1..3 vs. 1..3] does in this case contain a
   "disagree state" (2 vs. x), but at this point in the algorithm the only knowledge is that the [1..3 vs. 1..3]
   interval contains two "agree" states, we can't be sure to find any consecutive (agree,disagree) pair in that
   interval, because at that point in the execution of the algorithm, for all we know call the states in that interal
   could be all "agree" states.
   On the other hand, by looking at the interval [3..5 vs. 3..z], we can sure that two consecutive states
   [agree,disagree] exist within the interval, because we have [agree … unknown … disagree] so we already have the key
   ingredients (one of each). The states we have are not consecutive, so we will have to narrow down that interval until
   we get consecutive states.
   A discrete variation of the intermediate-value theorem tells us that if we start from an "agree" state (let's say it's
   encoded as a positive value) and end with a "disagree" state (let's say it's encoded as a negative value), at some point
   in that interval we will "cross the X axis of the chart", and we'll have two consecutive values, one just above the axis
   (positive = agree) and one just below (negative = disagree).
   So we continue our dichotomy in the [3..5 vs. 3..z] interval.
   We pick 4 vs. y, which is a "disagree" state, so we now have two sub-intervals [3..4 vs. 3..y] and [4..5 vs. y..z].
   The second sub-interval might not contain any "agree" (it's a [disagree .. unknown .. disagree] interval), but the first
   one is of the form [agree … unknown … disagree], again.
   So we pick [3..4 vs. 3..y] as our new interval.
   At this point, the two states are consecutive, so we found [3,4] vs. [3,y] which are consecutive [agree, disagree] states.
   We can now upload the relevant parts of the memory from 3, and execute just one single-step instruction on the CPU, and see
   if the correct result is 4 or y (or neither, which would mean that both A and B were liars).
   Q.E.D.
*)

(* the two participants in a litigation *)
type party = A | B

type param = (party * Merkle.hash)

let entryPoint (store : Todo.storage) (_step : param) : Todo.return =
  (* TODO *)
  let ops = ([] : operation list) in
  (ops, store)