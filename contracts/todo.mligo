(* TODO: this file is at the bottom of the dependency graph, definitions should
   be moved out but moving them out requires changing a few other definitions to avoid breaking
   a dependency. A bit of refactoring / better separation of concerns should take care of that. *)

type hash = bytes

type vm = {
  (* Merkle hash of the entire state of the off-chain VM (ROM + RAM + registers + I/O)
     The normal use is to have the instruction pointer in the zero-th
     location, pointing to the first location (ready to execute the first
     instruction of the code), followed by the code, followed by the storage. *)
  rootMerkleHash : hash ;
  (* log2(Size of all addressable memory i.e. ROM + RAM + registers + I/O).
     The size of addressable memory must always be a power of two, so that
     memorySizeLog is a (strictly positive) integer. In addition, there is a lower bound on
     the size of the memory. *)
  log2MemorySize : nat ;
}

let memorySize (vm : vm) = Bitwise.shift_left 1n vm.log2MemorySize

(* ID of a VM in the big_map  *)
type vmId = nat

(* ID of a litigation in the big_map  *)
type litigationId = nat

type operationRequest = {
    (* This is essentially the receipt of the off-chain execution of an off-chain contract.
       It contains the amount to be transferred, the recipient, an argument for the
       recepient contract, hash of the off-chain contract's input data,  *)
    amount    : tez;
    recipient : address;
    (* TODO: store the argument in a separate big_map, so that a big value doesn't
       slow down execution of the other entry points. *)
    argument  : bytes;
    (* TODO: instead of specifying the outputHash (must be the hash of argument) and amount and recipient,
       simply pass these as inputs and have the off-chain contract output only a boolean saying if its
       assumed output is correct or not. That means we don't have to compare the expected hash, recipient
       and amount, but instead can let the normal dichotomy on the contract execution find irregularities. *)
    inputHash : hash;
    outputHash : hash;
    (* off-chain contract, we store only a nat pointer
       to the merkle hash of the permanent VM running
       that contract *)
    vmId       : vmId;
    (* new VM state according to user A *)
    newVmRootHashA : hash;
}

type pendingOperation = {
    operationRequest : operationRequest;
    timelock         : timestamp;
}

type dichotomy = {
   traceRangeStart : nat;
   traceRangeEnd   : nat;
   (* invariant: both parties agree on the hash of the
      memory at the start of the range *)
   traceStartHash  : hash;
   (* invariant: the parties disagree on the hash of
      the memory at the end of the range *)
   traceEndHashA   : hash;
   traceEndHashB   : hash;
}

type litigation =
  | DichotomyInProgress of dichotomy
  | AwaitingStep of dichotomy

type storage = {
    nextId            : nat ;
    vms               : (nat, vm) big_map ;
    pendingOperations : (nat, pendingOperation) big_map ;
    litigations       : (nat, litigation) big_map ;
}

type return = operation list * storage
