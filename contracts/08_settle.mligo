#import "todo.mligo" "Todo"

(* since we simply cannot send an incorrect step to the chain, we have this entrypoint which allows the party in
   the wrong to "admit defeat" instead of waiting until the end of the timeout. Only messages which are truthful
   at a given stage of the trial are allowed, as soon as a lie is detected the message sent to this contract is
   rejected. This entry point is therefore a shortcut for sending a message which contains detectable lies. *)

type param = nat

let entryPoint (store : Todo.storage) (_id : param) : Todo.return =
  (* TODO *)
  let ops = ([] : operation list) in
  (ops, store)