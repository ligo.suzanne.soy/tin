(* Delay between a transfer request and the possibility to withdraw funds. *)
let transfer_delay = 86_400n

(* Length of the longest supported Merkle hash *)
let longest_hash = 64n

(* Minimum VM memory size (must have enough space for I/O, and to fit at least one full instruction) *)
let minLog2MemorySize = 10n
(* Maximum VM memory size (allows putting bounds on the number
   of transactions necessary to solve a complaint) *)
let maxLog2MemorySize = 64n

(* Length of the longest allowed callback argument (to avoid DOS attacks based on
   large argument values that let some entry points of this be executed but prevent
   executing other entry points) *)
let longest_argument = 100n
