#import "todo.mligo" "Todo"

let fresh_id (store : Todo.storage) : (Todo.storage * nat) =
  ({ store with nextId = store.nextId + 1n }, store.nextId)
