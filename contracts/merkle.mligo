#import "todo.mligo" "Todo"
#import "list_utils.mligo" "ListUtils"

type hash = Todo.hash

type siblingSide = LeftSibling | RightSibling

type sibling = siblingSide * hash

type leaf = {
    value : int ;
    index : nat ;
    (* path is a list of Merkle nodes, from leaf to root *)
    path  : hash list ;
}

type leaf_ = {
    value_ : int;
    (* index : nat ; *) (* The index is omitted, it will be obtained from other information *)
    (* path is a list of Merkle nodes, from leaf to root *)
    path_  : hash list;
}

(* Create a proper merkle leaf from a value by adding the missing info (address of the value) *)
let withIndex ({ value_ ; path_ } : leaf_) (index : nat) : leaf = {
  value = value_ ;
  path = path_ ;
  index = index ;
}

let hashMerkleNode (leftChild : hash) (rightChild : hash) : hash =
  (* Since the height of the tree is fixed, it is probably not 
     useful to concatenate a tag 0x01/0x02 to distinguish between
     hashing of a leaf and hashing of a concatenation of two hashes. *)
  Crypto.sha512 (Bytes.concat 0x02 (Bytes.concat leftChild rightChild))

let hashMerkleLeaf (leafValue : int) : hash =
  Crypto.sha512 (Bytes.concat 0x01 (Bytes.pack (leafValue : int)))

(* This represents the relevant parts of the entire Merkle tree, without having to allocate a large vector *)
type sparseVector = (nat, bytes) map

let emptySparseVector : sparseVector = Map.empty

(* `merkle` represents the entire Merkle tree (concatenated layers
    of inner nodes, starting with index 1 for the root). The leaves
    are not included, only their hashes at the last level.
    
    This function loads into `merkle` the hashes provided by `path`.
    
    e.g. for a tree with 8 leaves, we would have initially a sparse
    array
        merkle = [ ? ; ?;? ; ?;?;?;? ; ?;?;?;?;?;?;?;? ].
    
    Loading the leaf
        { value = … ; index = 5 ; path = [ 0xef ; 0xcd ; 0xab ] }
    would produce this sparse array:
        merkle = [ ? ; 0xab;? ; ?;?;?;0xcd ; ?;?;?;?;?;0xef;?;? ].
    which represents the partially-known tree:
                                (??)
                  ,--------------'`--------------,
                0xab                            (??)
          ,------''------,                ,------''------,
        ????            ????            (??)            0xcd
     ,---''---,      ,---''---,      ,---''---,      ,---''---,
    ????    ????    ????    ????    (??)    0xef    ????    ????
    
    where the nodes on the path to the leaf are marked as (??).
    
    This function also computes the (??) hashes, stores them in the
    tree, and compares them to the expected root hash.
    
    In addition, this function checks the merkle proof against an expected hash. *)
let checkAndLoadPath (vm : Todo.vm) (merkle : sparseVector) (leaf : leaf) =
  let expectedHash = vm.rootMerkleHash in
  let _index, xash, merkle =
    List.fold_left
      (fun ((index, xash, merkle : nat * hash * sparseVector), (siblingHash : hash)) ->
        let parentIndex = Bitwise.shift_right index 1n in
        let siblingIndex = Bitwise.xor index 1n in
        let xash =
          if Bitwise.and index 1n = 0n then
            hashMerkleNode xash siblingHash
          else
            hashMerkleNode siblingHash xash
        in
        (parentIndex, xash, Map.update siblingIndex (Some siblingHash) merkle))
      (Todo.memorySize vm + leaf.index, hashMerkleLeaf leaf.value, merkle)
      leaf.path
  in
  if xash = expectedHash then
    merkle
  else
    (* Incorrect Merkle proof, does not give the expected hash. *)
    (failwith "BadProof" : sparseVector)

let setPath (vm : Todo.vm) (merkle : sparseVector) (index : nat) (newValue : int) =
  let rec iter (merkle : sparseVector) (index : nat) (xash : hash)  (acc : hash list) : (sparseVector * hash list) =
    let siblingHash = Map.find_opt (Bitwise.xor index 1n) merkle in
    let siblingHash = match siblingHash with
      (* Given that we are only updating locations for which we already have a
         Merkle proof, we should always be able to get the siblings *)
      | None -> failwith "internal error"
      | Some h -> h in
    let (left, right) =
      if Bitwise.and index 1n = 0n then
        (xash, siblingHash)
      else
        (siblingHash, xash)
    in
    let xash = hashMerkleNode left right in
    let index = Bitwise.shift_right index 1n in
    let merkle = Map.update index (Some xash) merkle in
    if index = 1n then
      merkle, ListUtils.reverse (siblingHash :: acc)
    else
      iter merkle index xash (siblingHash :: acc)
  in
  let merkleIndex = (Todo.memorySize vm + index) in
  let xash = hashMerkleLeaf newValue in
  (* set the hash of the leaf Merkle node *)
  let merkle = Map.update merkleIndex (Some xash) merkle in
  let merkle, path = iter merkle merkleIndex xash ([] : hash list) in
  (merkle, { value = newValue ; index = index ; path = path; })
