let pop (type k v) (k : k) (m : (k,v) big_map) : v option * (k,v) big_map =
  Big_map.get_and_update k (None : v option) m
