(* To be changed along with protocol changes *)
let blockDuration = 30n

(* To be changed along with test framework changes *)
let blocksPerCycle = 12n

let cycleDuration = blockDuration * blocksPerCycle

let waitStep = cycleDuration

let waitExactly (seconds : nat) : unit = 
  let rest = seconds mod waitStep in
  if rest > 0n
    then failwith "The given number of seconds must be a multiple of cycleDuration"
  else
    Test.bake_until_n_cycle_end (seconds / waitStep)

(* returns the number of seconds actually spent (subject to imprecisions if the protocol doesn't do exactly 30 seconds per block?) *)
let waitAtLeast (seconds : nat) : nat = 
  let withMargin = seconds + (waitStep+1n)/2n in
  let roundedUp = (seconds / waitStep) + 1n in
  let () = Test.bake_until_n_cycle_end roundedUp in
  roundedUp
