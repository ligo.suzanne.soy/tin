(* CUT = Contract Under Test.
   We use this prefix to make it clear that this module is not part of the tests but is part of the contract being tested. *)
#import "../contracts/merkle.mligo" "CUT_Merkle"

type hash = bytes

let reverse (type a) (lst : a list) =
  List.fold_left (fun (acc,x : a list * a) -> x :: acc) ([] : a list) lst

let listConcat (type a) (x : a list) (y : a list) =
  List.fold_left (fun (acc,x : a list * a) -> x :: acc) y (reverse x)

let log2sup (x : nat) =
  let rec iter (acc : nat) : nat =
    if Bitwise.shift_left 1n acc >= x then
      acc
    else
      iter (acc + 1n)
  in
  iter 0n

type test_merkleTree = {
  test_log2MemorySize : nat;
  test_map : (nat, hash) map;
  test_root : hash;
  test_leaves : (nat, int) map;
}

let listToMap (type a) (l : a list) =
  let rec iter (index : nat) (l : a list) (acc : (nat, a) map) : (nat, a) map =
    match l with
    | [] -> acc
    | hd :: tl -> iter (index + 1n) tl (Map.add index hd acc)
  in
  iter 0n l (Map.empty : (nat, a) map)

(* This returns a tree with each layer concatenated to the previous one,
   with the root at index 1. e.g.
   [ 0x, root, left child, right child, left left, left right, right left, right right, … ] *)
let merkleTree (memory : int list) : test_merkleTree =
  let log2MemorySize = log2sup (List.length memory) in
  let () = assert (List.length memory = Bitwise.shift_left 1n log2MemorySize) in
  let hashes = List.map CUT_Merkle.hashMerkleLeaf memory in
  let rec hashPairs (hashes : hash list) (acc : hash list) : hash list =
    match hashes with
      | l :: r :: rest -> hashPairs rest (CUT_Merkle.hashMerkleNode l r :: acc)
      | single :: [] -> (failwith "Test error: odd number of elements in list" : hash list)
      | [] -> reverse acc
  in
  let rec iter (hashes : hash list) (acc : hash list) : test_merkleTree =
    match hashes with
    | [singleHash] ->
      (* Insert an empty entry in the list so that the root node is at index 1 (this makes navigating to parents by dividing by two easier) *)
      { 
        test_root           = singleHash;
        test_log2MemorySize = log2MemorySize;
        test_map            = listToMap (0x :: acc);
        test_leaves         = listToMap memory;
      }
    | manyHashes ->
      let newHashes = (hashPairs manyHashes ([] : hash list)) in
      iter newHashes (listConcat newHashes acc)
  in
  iter hashes (hashes : hash list)

let merkleProof (orig_index : nat) (merkleTree : test_merkleTree) =
(* Returns a Merkle inclusion proof, i.e. the sibbling hashes on the path from leaves to root *)
  let rec iter (index : nat) (acc : hash list) : CUT_Merkle.leaf_ =
    if index = 1n then
      match Map.find_opt orig_index merkleTree.test_leaves with
      | None -> failwith "Internal error in tests: couldn't access the value of the leaf in the function merkleProof"
      | Some value_ -> {
        path_ = reverse acc;
        value_ = value_
      }
    else
      let sibling = (Bitwise.xor 1n index) in
      match Map.find_opt sibling merkleTree.test_map with
      | None ->
        let () = Test.log sibling in
        failwith "Internal error in tests: couldn't find node in Merkle tree. Is the index correct and does the tree contain all nodes?"
      | Some merkleNode -> iter (Bitwise.shift_right index 1n) (merkleNode :: acc)
  in
  iter ((Bitwise.shift_left 1n merkleTree.test_log2MemorySize) + orig_index) ([] : hash list)
