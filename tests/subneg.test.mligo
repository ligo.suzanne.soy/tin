#import "../contracts/subneg.mligo" "CUT_Subneg"
#import "../contracts/settings.mligo" "CUT_Settings"
#import "../contracts/step.mligo" "CUT_Step"
#import "../contracts/todo.mligo" "CUT_Todo"
#import "../contracts/recipient.mligo" "Recipient"
#import "test_utils.mligo" "TestUtils"
#import "test_utils_merkle.mligo" "TestUtilsMerkle"

let emptyStorage = CUT_Subneg.emptyStorage

let testInitialStorage =
  let initialStorage = emptyStorage in
  let (taddr, _, _) = Test.originate CUT_Subneg.main initialStorage 0tez in
  assert (Test.get_storage taddr = initialStorage)

(*

  | StartTransfer of startTransferParam
  | Complaint of complaintParam
  | FinishTransfer of finishTransferParam

*)

let startTransfer =
  let initialStorage = emptyStorage in
  let (taddr, _, _) = Test.originate CUT_Subneg.main initialStorage 0tez in
  let contr = Test.to_contract taddr in
  let (recipient, _, _) = Test.originate Recipient.main Recipient.emptyStorage 0tez in
  let recipientAddress = Tezos.address (Test.to_contract recipient) in
  let operationRequest = {
    amount    = 1tez;
    recipient = recipientAddress;
    argument  = 0xcafe;
    inputHash = Crypto.sha512 0x01;
    outputHash = Crypto.sha512 0x02;
    vmId      = 0n;
    newVmRootHashA = Crypto.sha512 0x03 ;
  } in
  let start_timestamp = (86400 : timestamp) in
  (* let _unit = Test.set_now start_timestamp in *)
  (* TODO: have a separate entry point to send funds to an account? Or maybe the Tezos.amount should be given as an input to the off-chain contract. *)
  let _gas = Test.transfer_to_contract_exn contr (StartTransfer operationRequest) 1tez in
  let storage = Test.get_storage taddr in
  let operationRequestId = abs(storage.nextId - 1n) in
  (*let _unit = Test.set_now (start_timestamp + CUT_Settings.transfer_delay + 1) in*)
  let _waited = TestUtils.waitAtLeast CUT_Settings.transfer_delay in
  let () = assert (Test.get_balance recipientAddress = 0tez) in
  let _gas = Test.transfer_to_contract_exn contr (FinishTransfer operationRequestId) 0tez in
  assert (Test.get_balance recipientAddress = 1tez)

let step =
  (* Address Meaning               Value Comment
     0       Instruction pointer  = 1
     
             Fist instruction: SUBNEG 7 7 4, meaning mem[7] := mem[7] - mem[7]; if mem[7] <= 0 then goto 4 else goto IP+3
     1       First instruction A  = 7     Address of the "x" memory cell (initial value: 42)
     2       First instruction B  = 7     Address of the "x" memory cell (initial value: 42)
     3       First instruction C  = 4     Address of the second instruction

             Second instruction: SUBNEG 7 7 1, meaning mem[7] := mem[7] - mem[7]; if mem[7] <= 0 then goto 1 else goto IP+3
     4       Second instruction A = 7     Address of the "x" memory cell (initial value: 42)
     5       Second instruction B = 7     Address of the "x" memory cell (initial value: 42)
     6       Second instruction C = 1     Address of the first instruction
     
     7       "x" memory cell      = 42
     *)
  (*              [ 0   1   2   3   4   5   6   7  ]   *)
  let memBefore = [ 1 ; 7 ; 7 ; 4 ; 7 ; 7 ; 1 ; 42 ] in
  (* TODO : we shouldn't be able to pick the instruction pointer address!!!! *)
  let memAfter = [ 4 ; 7 ; 7 ; 4 ; 7 ; 7 ; 1 ; 0 ] in
  (* let () = Test.log (TestUtilsMerkle.merkleTree memBefore) in *)
  (* let () = Test.log (TestUtilsMerkle.merkleTree memAfter) in *)
  (* let () = Test.log (TestUtilsMerkle.merkleProof 0n (TestUtilsMerkle.merkleTree memBefore)) in *)
  let tree = (TestUtilsMerkle.merkleTree memBefore) in
  let expectedTree = (TestUtilsMerkle.merkleTree memAfter) in
  let vm : CUT_Todo.vm = {
    rootMerkleHash = tree.test_root;
    log2MemorySize = 3n ;
  } in
  let stepData : CUT_Step.step = {
    vmId                  = 0n; (* unused by this function *)
    litigationId          = 0n; (* unused by this function *)
    oldInstructionPointer = (TestUtilsMerkle.merkleProof 0n tree);
    oldA                  = (TestUtilsMerkle.merkleProof 1n tree);
    oldB                  = (TestUtilsMerkle.merkleProof 2n tree);
    oldC                  = (TestUtilsMerkle.merkleProof 3n tree);
    oldMemA               = (TestUtilsMerkle.merkleProof 7n tree);
    oldMemB               = (TestUtilsMerkle.merkleProof 7n tree);
  } in
  let rootAfterStep = CUT_Step.step vm stepData in
  let () = assert (expectedTree.test_root = rootAfterStep) in
  ()
